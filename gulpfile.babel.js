import { parallel, series } from 'gulp';
import { Pug } from './gulp/tasks/pug';
import { Serve } from './gulp/tasks/server';
import { WatchFiles } from './gulp/tasks/watch';
import { StyleVendor, Style } from './gulp/tasks/style';
import { Script, ScriptVendor } from './gulp/tasks/script';
import { Font, Icon } from './gulp/tasks/font';
import { Image } from './gulp/tasks/image';

// Сервер + watch
let RunServer = parallel(
    WatchFiles,
    Serve
);

// Dev-сборка
let Build = series(
    Image,
    Font,
    ScriptVendor,
    Script,
    StyleVendor,
    Style,
    Pug,
    RunServer
);

// Prod-сборка
let BuildProd = series(
    Image,
    Font,
    ScriptVendor,
    Script,
    StyleVendor,
    Style,
    Pug
);

exports.icon        = Icon;
exports.default     = Build;
exports.production  = BuildProd;
