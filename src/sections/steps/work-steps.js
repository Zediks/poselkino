let step = $('.step'),
    stepCircle = step.find('.step-circle'),
    stepTitle = step.find('.step-title'),
    stepNext = step.find('.step-cloud__next'),
    stepPrev = step.find('.step-cloud__prev');

stepCircle.on('click', function() {
    step.removeClass('active');
    $(this).parent('.step').addClass('active');
});

stepTitle.on('click', function() {
    let step = $('.step');
    step.removeClass('active');
    $(this).parent('.step').addClass('active');
});

stepNext.on('click', function() {
    let activeStep = $('.step.active'),
        activeStepNum = activeStep.data('step');

        if (activeStepNum !==6) {
            activeStep.removeClass('active');
            $('.step[data-step="'+(activeStepNum+1)+'"]').addClass('active');
        }
});
stepPrev.on('click', function() {
    let activeStep = $('.step.active'),
        activeStepNum = activeStep.data('step');

    if (activeStepNum !==1) {
        activeStep.removeClass('active');
        $('.step[data-step="'+(activeStepNum-1)+'"]').addClass('active');
    }
});
