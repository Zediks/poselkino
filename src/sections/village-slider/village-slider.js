$('#village-slider').not('.slick-initialized').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,

});

$('#village-slider-thumb').not('.slick-initialized').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '#village-slider',
    dots: false,
    focusOnSelect: true,
    prevArrow:"<button type='button' class='slick-prev' aria-label='Скролл влево'><<svg xmlns='http://www.w3.org/2000/svg' width='12.552' height='22.81'><g id='next' transform='translate(-111.989 .567)'><path d='M123.864 10.438L113.607.181a.615.615 0 00-.87.87l9.82 9.82-9.82 9.82a.617.617 0 00.433 1.053.6.6 0 00.433-.182L123.86 11.3a.612.612 0 00.004-.862z' fill='#fff' stroke='#fff' stroke-width='1'/></g></svg></button>",
    nextArrow:"<button type='button' class='slick-next' aria-label='Скролл вправо'><svg xmlns='http://www.w3.org/2000/svg' width='12.552' height='22.81'><g id='next' transform='translate(-111.989 .567)'><path d='M123.864 10.438L113.607.181a.615.615 0 00-.87.87l9.82 9.82-9.82 9.82a.617.617 0 00.433 1.053.6.6 0 00.433-.182L123.86 11.3a.612.612 0 00.004-.862z' fill='#fff' stroke='#fff' stroke-width='1'/></g></svg></button>",
    responsive: [
        {
            breakpoint: 1199,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});

$('#mobile-nav-slider').not('.slick-initialized').slick({
    infinite: false,
    variableWidth: true,
    arrows: false,
    dots: false,
});