// Добавляем активный класс по клику
$('.address__tab .nav-link').on('click', function() {
    $(this).addClass('active');
    $('.address__tab .nav-link').removeClass('active');
})

// Показываем все адреса
$('#showCollapseAddress').on('click', function() {
    $('.address__tab .tab-content').toggleClass('showToggle');
});