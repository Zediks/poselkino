// Carousel photo
function getInsta() {
    return { 
        infinite: true,
        variableWidth: true,
        slidesToScroll: 1,
        arrows: false,
        fots: false
    }
}

$('#instaSlider').not('.slick-initialized').slick(getInsta());