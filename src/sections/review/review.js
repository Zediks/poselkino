// Добавляем активный класс по клику
$('.review-list .nav-link').on('click', function() {
    $(this).addClass('active');
    $('.review-list .nav-link').removeClass('active');
})

// Slider Address tab
function getReviews() {
    return {
        infinite: false,
        variableWidth: true,
        arrows: false,
        dots: false
    }
}
$('#reviewList').not('.slick-initialized').slick(getReviews());

$('.star-list label').on('click mouseover', function() {
    var value = $(this).next('input').attr('value');
    console.log(value);
    $('span.star-value__title').html(value);
});

$('.star-list label').on('click', function() {
    $('.star-list label').removeClass('checked');
    $(this).addClass('checked');
});