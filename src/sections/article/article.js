function getArticle() {
    return { 
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        prevArrow:"<button type='button' class='slick-prev' aria-label='Скролл влево'><svg xmlns='http://www.w3.org/2000/svg' width='15.574' height='9.815' viewBox='0 0 15.574 9.815'><path d='M.657 79.964h12.275L10 77.035a.657.657 0 0 1 .929-.929l4.05 4.05a.657.657 0 0 1 0 .929l-4.05 4.051a.657.657 0 0 1-.929-.929l2.929-2.929H.657a.657.657 0 1 1 0-1.314z' transform='translate(0 -75.914)'></svg></button>",
        nextArrow:"<button type='button' class='slick-next' aria-label='Скролл вправо'><svg xmlns='http://www.w3.org/2000/svg' width='15.574' height='9.815' viewBox='0 0 15.574 9.815'><path d='M.657 79.964h12.275L10 77.035a.657.657 0 0 1 .929-.929l4.05 4.05a.657.657 0 0 1 0 .929l-4.05 4.051a.657.657 0 0 1-.929-.929l2.929-2.929H.657a.657.657 0 1 1 0-1.314z' transform='translate(0 -75.914)'></svg></button>",
        touchMove: false,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    }
}

$('#slider_article').not('.slick-initialized').slick(getArticle());