$('.credit__slider-range .slider-range-thousands').on('change', function() {
    let valueCredit = $(this).val().replace(/(\d{1,3})(?=((\d{3})*)$)/g, " $1");
    $(this).parent().prev().find('span').text(valueCredit);
})

$('.credit__slider-range .slider-range-percent').on('change', function() {
    let valueCredit = $(this).val().replace('.', ',')
    $(this).parent().prev().find('span').text(valueCredit);
})

$('.credit__slider-range .slider-range-year').on('change', function() {
    let valueCredit = $(this).val()
    $(this).parent().prev().find('span').text(valueCredit);
})