ymaps.ready(init);
    function init(){
        var myMap = new ymaps.Map("pageMapContainer", {
        center: [55.76, 37.64],
        zoom: 7 
    });

    var myClusterer = new ymaps.Clusterer({
        preset: 'islands#redClusterIcons',
    });
    var imageIcon = '/assets/img/site/placeholder-map.svg';

    var myPlacemark_1855 = new ymaps.Placemark([56.006097, 37.510635], {
        hintContent: 'Аббакумово',
        id: 'mark_1855',
    },{
        iconLayout: 'default#image',
        iconImageHref: imageIcon,
        iconImageSize: [30, 42],
    });

    // клик по метке
    myPlacemark_1855.events.add('click', function() {
        $('.card-map').hide(0);
        $('#form-map-1855').show(0);
        $('#form-map-1855').css({'z-index':'99999999999'});
    });
    
    var myPlacemark_1579 = new ymaps.Placemark([56.200874, 37.241269], {
        hintContent: 'Солнечный лес',
        id: 'mark_1579',
    }, {
        iconLayout: 'default#image',
        iconImageHref: imageIcon,
        iconImageSize: [30, 42],
    });


    // клик по метке
    myPlacemark_1579.events.add('click', function() {
        $('.card-map').hide(0);
        $('#form-map-1579').show(0);
        $('#form-map-1579').css({'z-index':'99999999999'});
    });

    var myPlacemark_668 = new ymaps.Placemark([56.233694, 37.571300], {
        hintContent: 'Дмитровские дачи',
        id: 'mark_668',
    }, {
        iconLayout: 'default#image',
        iconImageHref: imageIcon,
        iconImageSize: [30, 42],
    });

    // клик по метке
    myPlacemark_668.events.add('click', function() {
        $('.card-map').hide(0);
        $('#form-map-668').show(0);
        $('#form-map-668').css({'z-index':'99999999999'});
    });
    
    myMap.geoObjects.add(myPlacemark_1855);
    myClusterer.add(myPlacemark_1855);
    myMap.geoObjects.add(myPlacemark_1579);
    myClusterer.add(myPlacemark_1579);
    myMap.geoObjects.add(myPlacemark_668);
    myClusterer.add(myPlacemark_668);
    myMap.geoObjects.add(myClusterer);
}