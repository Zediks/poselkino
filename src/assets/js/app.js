
$( document ).ready(function() {
    // @include('./bootstrap-validate.js')
    $('select').selectric();

    autosize($('textarea'));
    
    // Navbar toggl
    $('.navbar-toggler').on('click', function (callback) {
        $('.header__navbar').toggleClass('show');
        $(this).attr('data-expanded', $('.navbar-toggler').attr('data-expanded') == 'true' ? 'false' : 'true');
        var noScroll = function noScroll () {
            $('body').toggleClass('no-scroll');
        };
        setTimeout(noScroll, 1000);
    });

    // Offer 
    // @include('../../components/card/card.js')

    // Mobile hide filter
    // @include('../../sections/filter/filter.js')

    // Tag list
    // @include('../../sections/tag-list/tag-list.js')

    // Tab address
    // @include('../../sections/address-tab/address-tab.js')
    
    // Article
    // @include('../../sections/article/article.js')

    // Hero
    // @include('../../sections/hero/hero.js')

    // Plan
    // @include('../../sections/plan/plan.js')

    // Work steps
    // @include('../../sections/steps/work-steps.js')
    
    // Review tabs mobile scroll
    // @include('../../sections/review/review.js')

    // Vilage Slider
    // @include('../../sections/village-slider/village-slider.js')

    // Photo buttons steps
    // @include('../../sections/card/house.js')

    // Legal information
    // @include('../../sections/legal-information/legal-information.js')

    // Footer Slider
    // @include('../../sections/footer/footer.js')

    // Credit
    // @include('../../sections/credit/credit.js')

    // Search
    // @include('../../sections/header/header.js')

    // Archor
    $('.anchor a').on('click', function (e) {
        e.preventDefault();
        $('.anchor a').removeClass('.btn-success').addClass('.btn-outline-success');
        $(this).addClass('.btn-success').removeClass('.btn-outline-success');
        var id  = $(this).attr('href'),
            top = $(id).offset().top;

            console.log('id' + id);
            
        $('body,html').animate({scrollTop: top}, 1500);
    });

    // Blog navigation
    $('.blog-navigation').not('.slick-initialized').slick({
        arrows: false,
        dots: false,
        infinite: false,
        variableWidth: true,
    });

    // House Modal
    $('.house-modal').on('click', function(){
       $('#houseModal').css('display', 'block');
       $('body').addClass('modal-open');
       $('body').prepend('<div class="modal-backdrop show"></div');
    });

    $('button[data-dismiss="modal"]').on('click', function(){
        $('#houseModal').css('display', 'none');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    });

    // Phone
    $(".phone").inputmask({"mask": "+7 (999) 999-9999", "placeholder": ""});


    // Range modal (Площадь дома)
    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 3000,
        values: [ 0, 2164 ],
        slide: function( event, ui ) {
            $( "#modalAreaStart" ).val( ui.values[ 0 ] );
            $( "#modalAreaEnd" ).val( ui.values[ 1 ] );
        }
    });

    $('#modalAreaEnd').val($('#slider-range').slider('values', 1));

    // Воспроизведение большого видео
    $(function() {
        $('.video svg.play').on('click', function() {
            var dataYoutube = $(this).parents('.video.radius').data('youtube');
            $(this).parents('.video.radius').html('<iframe src="https://www.youtube.com/embed/'+dataYoutube+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>')
        });
    });

    // Интерфейс фильтра
    $('#toggleHighway, #toggleHighwayBack, #highwayDone').on('click', function () {
        $('.map-filter--highway').toggleClass('show');
        $('.map-filter__content').toggleClass('hide');
    });
    
    // Интерфейс фильтра
    $('#toggleAreas, #toggleAreasBack, #areasDone').on('click', function () {
        $('.map-filter--areas').toggleClass('show');
        $('.map-filter__content').toggleClass('hide');
    });
    
    $('.highway-direction').on('click', function () {
        var direction = $(this).data('direction');

        if ( $('.map-filter--highway input[data-direction="'+ direction+'"]').is(':checked') ) {
            $('.map-filter--highway input[data-direction="'+ direction+'"]').prop('checked', false);
        } else {
            $('.map-filter--highway input[data-direction="'+ direction+'"]').prop('checked', true);
        }
    })
    $('.areas-direction').on('click', function () {
        var direction = $(this).data('direction');

        if ( $('.map-filter--areas input[data-direction="'+ direction+'"]').is(':checked') ) {
            $('.map-filter--areas input[data-direction="'+ direction+'"]').prop('checked', false);
        } else {
            $('.map-filter--areas input[data-direction="'+ direction+'"]').prop('checked', true);
        }
    })

    $('.page-map .show-filter').on('click', function () {
        $(this).toggle()
        $('.page-map__filter').toggleClass('show')
    })
    $('.page-map .close-filter').on('click', function () {
        $('.page-map .show-filter').show()
        $('.page-map__filter').toggleClass('show')
    })
});

$('.card-map .close-map').on('click', function() {
    $(this).parent('.card-map').toggle()
})

$('#video-gallery').lightGallery(); 