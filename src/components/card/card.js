// Carousel photo
function getPhoto() {
    return { 
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow:"<button type='button' class='slick-prev' aria-label='Скролл влево'><svg xmlns='http://www.w3.org/2000/svg' width='12.552' height='22.81'><g id='next' transform='translate(-111.989 .567)'><path d='M123.864 10.438L113.607.181a.615.615 0 00-.87.87l9.82 9.82-9.82 9.82a.617.617 0 00.433 1.053.6.6 0 00.433-.182L123.86 11.3a.612.612 0 00.004-.862z' fill='#fff' stroke='#fff' stroke-width='1'></path></g></svg></button>",
        nextArrow:"<button type='button' class='slick-next' aria-label='Скролл вправо'><svg xmlns='http://www.w3.org/2000/svg' width='12.552' height='22.81'><g id='next' transform='translate(-111.989 .567)'><path d='M123.864 10.438L113.607.181a.615.615 0 00-.87.87l9.82 9.82-9.82 9.82a.617.617 0 00.433 1.053.6.6 0 00.433-.182L123.86 11.3a.612.612 0 00.004-.862z' fill='#fff' stroke='#fff' stroke-width='1'></path></g></svg></button>",
        touchMove: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: true,
                }
            },
        ]
    }
}

// Slider card
function getCard() {
    return { 
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        prevArrow:"<button type='button' class='slick-prev' aria-label='Скролл влево'><svg xmlns='http://www.w3.org/2000/svg' width='15.574' height='9.815' viewBox='0 0 15.574 9.815'><path d='M.657 79.964h12.275L10 77.035a.657.657 0 0 1 .929-.929l4.05 4.05a.657.657 0 0 1 0 .929l-4.05 4.051a.657.657 0 0 1-.929-.929l2.929-2.929H.657a.657.657 0 1 1 0-1.314z' transform='translate(0 -75.914)'></svg></button>",
        nextArrow:"<button type='button' class='slick-next' aria-label='Скролл вправо'><svg xmlns='http://www.w3.org/2000/svg' width='15.574' height='9.815' viewBox='0 0 15.574 9.815'><path d='M.657 79.964h12.275L10 77.035a.657.657 0 0 1 .929-.929l4.05 4.05a.657.657 0 0 1 0 .929l-4.05 4.051a.657.657 0 0 1-.929-.929l2.929-2.929H.657a.657.657 0 1 1 0-1.314z' transform='translate(0 -75.914)'></svg></button>",
        touchMove: false,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                }
            },
        ]
    }
}
// Slider Address tab
function getAddress() {
    return {
        infinite: false,
        variableWidth: true,
        prevArrow:"<button type='button' class='slick-prev' aria-label='Скролл влево'><svg xmlns='http://www.w3.org/2000/svg' width='15.574' height='9.815' viewBox='0 0 15.574 9.815'><path d='M.657 79.964h12.275L10 77.035a.657.657 0 0 1 .929-.929l4.05 4.05a.657.657 0 0 1 0 .929l-4.05 4.051a.657.657 0 0 1-.929-.929l2.929-2.929H.657a.657.657 0 1 1 0-1.314z' transform='translate(0 -75.914)'></svg></button>",
        nextArrow:"<button type='button' class='slick-next' aria-label='Скролл вправо'><svg xmlns='http://www.w3.org/2000/svg' width='15.574' height='9.815' viewBox='0 0 15.574 9.815'><path d='M.657 79.964h12.275L10 77.035a.657.657 0 0 1 .929-.929l4.05 4.05a.657.657 0 0 1 0 .929l-4.05 4.051a.657.657 0 0 1-.929-.929l2.929-2.929H.657a.657.657 0 1 1 0-1.314z' transform='translate(0 -75.914)'></svg></button>"
    }
}
// Slider cardHouseCarousel
function cardHouseCarousel() {
    return { 
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        prevArrow:"<button type='button' class='slick-prev' aria-label='Скролл влево'><svg xmlns='http://www.w3.org/2000/svg' width='15.574' height='9.815' viewBox='0 0 15.574 9.815'><path d='M.657 79.964h12.275L10 77.035a.657.657 0 0 1 .929-.929l4.05 4.05a.657.657 0 0 1 0 .929l-4.05 4.051a.657.657 0 0 1-.929-.929l2.929-2.929H.657a.657.657 0 1 1 0-1.314z' transform='translate(0 -75.914)'></svg></button>",
        nextArrow:"<button type='button' class='slick-next' aria-label='Скролл вправо'><svg xmlns='http://www.w3.org/2000/svg' width='15.574' height='9.815' viewBox='0 0 15.574 9.815'><path d='M.657 79.964h12.275L10 77.035a.657.657 0 0 1 .929-.929l4.05 4.05a.657.657 0 0 1 0 .929l-4.05 4.051a.657.657 0 0 1-.929-.929l2.929-2.929H.657a.657.657 0 1 1 0-1.314z' transform='translate(0 -75.914)'></svg></button>",
        touchMove: false,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                }
            },
        ]
    }
}
$('.card-house-carousel').not('.slick-initialized').slick(cardHouseCarousel());
$('#special_offers').not('.slick-initialized').slick(getCard());
$('#raiting-area-home-slick').not('.slick-initialized').slick(getCard());
$('#raiting-area-slick').not('.slick-initialized').slick(getCard());
$('#addressTab').not('.slick-initialized').slick(getCard());
$('#addressTab').not('.slick-initialized').slick(getAddress());
$('#similar_houses').not('.slick-initialized').slick(getCard());
$('#house_in_village').not('.slick-initialized').slick(getCard());

// Счетчик
// Поселки с высоким рэйтингом
$('.photo__list, .card-photo__list').not('.slick-initialized').slick(getPhoto()).on('init reInit afterChange', function(event,  slick, currentSlide, nextSlide) {
    var counter = $(this).parent('.photo').find('.photo__count .current'); 
    if ( currentSlide ) {
        console.log(currentSlide, counter);
        
        counter.text(currentSlide + 1);
    } else {
        counter.text(1);
    }
});