import { src, dest } from 'gulp';
import { path } from '../path';
import env from 'gulp-environment';
import pug from 'gulp-pug';
import bs from 'browser-sync';
import embedSvg from 'gulp-embed-svg';

global.$ = {
    fs: require('fs')
};

export const Pug = (done) => {
    src(path.pug.src)
        .pipe(pug({
            pretty: '    ',
            locals: {
                address: JSON.parse($.fs.readFileSync('./data/address.json', 'utf8')),
                arrangement: JSON.parse($.fs.readFileSync('./data/arrangement.json', 'utf8')),
            }
        }))
        .pipe(embedSvg({
            selectors: '.inline-svg',
            root: './src/assets/'
          }))
        .pipe(dest(path.pug.dist))
        .pipe(env.if.development(bs.reload({stream: true})))
        done();
}