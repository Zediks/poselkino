import { src, dest }        from 'gulp';
import { path }             from '../path';
import env                  from 'gulp-environment';
import bs                   from 'browser-sync';

// Перебрасываем шрифты в статику
export const Font = (done) => {
    src(path.font.src)
        .pipe(dest(path.font.dist))
        .pipe(env.if.development(bs.reload({stream: true})))
        done();
};
