import { watch, parallel } from 'gulp';
import { path } from '../path';
import { Pug } from './pug';
import { Style, StyleVendor } from './style';
import { Script, ScriptVendor } from './script';
import { Font, Icon } from './font';
import { Image } from './image';

// Слежение за изменяемыми файлами
export const WatchFiles = (done) => {
    // watch('Путь', tasks)
    watch(path.pug.watch, Pug);
    watch(path.style.watch, parallel(Style, StyleVendor));
    watch(path.script.watch, parallel(Script, ScriptVendor));
    watch(path.font.watch, Font);
    watch(path.icon.watch, Icon);
    watch(path.image.watch, Image);
    done();
}
