import { src, dest }        from 'gulp';
import { path }             from '../path';
import env                  from 'gulp-environment';
import bs                   from 'browser-sync';
import autoprefixer         from 'gulp-autoprefixer';
import concat               from 'gulp-concat';
import cssnano              from 'gulp-cssnano';
import postcss              from 'gulp-postcss';
import postcssReporter      from 'postcss-reporter';
import postcssScss          from 'postcss-scss';
import scss                 from 'gulp-sass';
import sourcemaps           from 'gulp-sourcemaps';
import stylelint            from 'stylelint';

// Сборка основных стилей
export const Style = (done) => {
    src(path.style.src)
        .pipe(env.if.development(sourcemaps.init()))
        .pipe(scss())
        .pipe(autoprefixer())
        .pipe(concat('style.min.css'))
        .pipe(cssnano())
        .pipe(env.if.development(sourcemaps.write('../maps')))
        .pipe(dest(path.style.dist))
        .pipe(env.if.development(bs.stream()))
        done();
}

// Сборка подключаемых стилей
export const StyleVendor = (done) => {
    src(path.style.srcVendor)
        .pipe(scss())
        .pipe(concat('vendor.min.css'))
        .pipe(cssnano())
        .pipe(dest(path.style.dist))
        .pipe(env.if.development(bs.stream()))
        done();
}
