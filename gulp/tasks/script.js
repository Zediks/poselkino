import { src, dest }    from 'gulp';
import { path }         from '../path';
import env              from 'gulp-environment';
import bs               from 'browser-sync';
import fileInclude      from 'gulp-file-include';
import eslint           from 'gulp-eslint';
import sourcemaps       from 'gulp-sourcemaps';
import concat           from 'gulp-concat';
import minify           from 'gulp-minify';
// Основной скрипт
export const Script = (done) => {
    src(path.script.src)
        .pipe(env.if.development(sourcemaps.init()))
        .pipe(fileInclude({
            prefix: '// @',
        }))
        .pipe(env.if.development(sourcemaps.write('../maps')))
        .pipe(dest(path.script.dist))
        .pipe(env.if.development(bs.reload({stream: true})))
        done();

}

// Создание скрипта с подключаемыми библиотеками
export const ScriptVendor = (done) => {
    src(path.script.srcVendor)
        .pipe(fileInclude({
            prefix: '// @',
        }))
        .pipe(concat('vendor.min.js'))
        .pipe(dest(path.script.dist))
        .pipe(env.if.development(bs.reload({stream: true})))
        done();
}
