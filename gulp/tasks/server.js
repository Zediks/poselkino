import bs from 'browser-sync';
import { path } from '../path';

// Локальный сервер
export const Serve = (done) => {
    bs.init({
        server: {
            baseDir: path.build,
            index: '/index.html' // Добавляем папку, т.к. пока единственная страница
        },
        ghostMode: true, // Дублирует поведение на всех страницах
        notify: false, // Вслывашка о старте сервера
        host: '0.0.0.0',
        port: 9000
    });

    done();
};