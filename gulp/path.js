export const path = {
    build: './public',
    pug: {
        src: [
            './src/pages/*.pug',
            './src/pages/**/*.pug'
        ],
        dist: './public',
        watch: [
            './src/pages/*.pug',
            './src/pages/**/*.pug',
            './src/layouts/*.pug',
            './src/sections/**/*.pug',
            './src/components/*.pug',
            './src/components/**/*.pug',
            './data//**/*.json',
        ]
    },
    style: {
        src: './src/assets/scss/style.scss',
        srcVendor: './src/assets/scss/vendor.scss',
        dist: './public/assets/css/',
        watch: [
            './src/assets/scss/*.scss',
            './src/components/**/*.scss',
            './src/sections/**/*.scss'
        ]
    },
    script: {
        src: [
            './src/assets/js/app.js',
            './src/sections/map/map-village.js',
            './src/assets/js/map.js'
        ],
        srcVendor: './src/assets/js/vendor.js',
        dist: './public/assets/js/',
        watch: [
            './src/assets/js/*.js',
            './src/components/**/*.js',
            './src/sections/**/*.js'
        ]
    },
    font: {
        src: './src/assets/fonts/**/*.*',
        dist: './public/assets/fonts/',
        watch: './src/assets/fonts/**/*.*'
    },
    icon: {
        src: './src/assets/img/icons/*.svg',
        dist: './src/assets/fonts/icons/',
        targetPath: '../../scss/_icon.scss',
        fontPath: '../fonts/icons/',
        watch: './src/assets/img/icons/*.svg'
    },
    image: {
        src: [
            './src/assets/img/**/*.*',
            '!./src/assets/img/icons/*.*'
        ],
        dist: './public/assets/img/',
        watch: './src/assets/img/**/*.*'
    },
};
