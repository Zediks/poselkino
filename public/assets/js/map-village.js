function loadMaps() {
    ymaps.ready(function () {
        var myMap = new ymaps.Map('villageMap', {
            center: [55.733835, 37.588227],
            zoom: 12,
            controls: []
        });
    
        var myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {
            preset: 'islands#redDotIcon'
        });
    
        myMap.geoObjects.add(myPlacemark);
        myMap.behaviors.disable('scrollZoom');
    });
};

setTimeout(loadMaps, 3000);
//# sourceMappingURL=../maps/map-village.js.map
